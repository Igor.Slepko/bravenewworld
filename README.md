# README

## Name
Trainee Igor Slepko project

## Description

WA enrolling to autoQA related project.

Consists of different .py files for locust tool.

To use files: add them to IdeaProjects/<project name> directory. Files with name 'locustfile.py' are selected for execution by default.

To launch a file with different name use -f 'filename' command

Example: 

locust --headless -u 10 -r 0.5 -t 10s -H localhost:3000

means start default 'locustfile' file for 10 users with 0.5 spawn rate for 10 seconds targeting localhost:3000 site

