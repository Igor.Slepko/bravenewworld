from locust import HttpUser, task


class RequestGoogle(HttpUser):
    @task
    def google_doodles(self):
        with self.client.get("/doodles") as r:
            assert r.status_code == 200, 'Expected 200 but error ' + str(r.status_code)

    @task
    def google_search_locust(self):
        with self.client.get("/search?q=locust") as r:
            assert r.status_code == 200, 'Expected 200 but error ' + str(r.status_code)
            assert r.content is not None, 'Expected response is not empty, but empty'